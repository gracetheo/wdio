import { ChainablePromiseElement } from 'webdriverio';

import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
    /**
     * define selectors using getter methods
     */
    public get furboBtn(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $('button[type="button"]');
    }

    public get email(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $('input[name="email"]');
    }

    public get password(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $('input[name="password"]');
    }

    public get invalidInputMsg(): ChainablePromiseElement<Promise<WebdriverIO.Element>>  {
        return $('p[class*="MuiFormHelper"]');
    }

    public get submitBtn(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $('button[type="submit"]');
    }

    public get forgotPasswordLink(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $('a[href="/forgotpassword"]');
    }

    public get rememberMeBtn(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $('input[type="checkbox"]');
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    public async login (): Promise<void> {
        await this.submitBtn.click();
    }

    public async inputEmail (username: string): Promise<void> {
        await this.email.setValue(username);
    }

    public async inputPassword (password: string): Promise<void> {
        await this.password.setValue(password);
    }

    public async gotoForgotPassword (): Promise<void> {
        await this.forgotPasswordLink.click();
    }

    public async gotoFurbo (): Promise<void> {
        await this.furboBtn.click();
    }

    public async rememberMe (): Promise<void> {
        await this.rememberMeBtn.click();
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    public open(): Promise<string> {
        return super.open('account');
    }
}

export default new LoginPage();
