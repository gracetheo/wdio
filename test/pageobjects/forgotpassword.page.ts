import { ChainablePromiseElement, ChainablePromiseArray } from 'webdriverio';

import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class ForgotPasswordPage extends Page {
    /**
     * define selectors using getter methods
     */
    public get email(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $('input[name="email"]');
    }

    public get buttons(): ChainablePromiseArray<WebdriverIO.ElementArray> {
        return $$('button[type="button"]');
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */

    public async returnHome (): Promise<void> {
        await this.buttons[1].click();
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    public open(): Promise<string> {
        return super.open('forgotpassword');
    }
}

export default new ForgotPasswordPage();
