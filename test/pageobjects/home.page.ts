import { ChainablePromiseElement, ChainablePromiseArray } from 'webdriverio';

import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
    /**
     * define selectors using getter methods
     */
    public get email(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $('input[name="email"]');
    }

    public get buttons(): ChainablePromiseArray<WebdriverIO.ElementArray> {
        return $$('button[type="button"]');
    }

    public get dropDowns(): ChainablePromiseArray<WebdriverIO.ElementArray> {
        return $$('div[role="none"]');
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    public async gotoFurbo(): Promise<void> {
        await this.buttons[0].click();
    }

    public async menu(): Promise<void> {
        await this.buttons[1].click();
    }

    public async gotoSub(): Promise<void> {
        await this.buttons[2].click();
    }

    public async gotoFDN(): Promise<void> {
        await this.buttons[3].click();
    }

    public async menuAccount(): Promise<void> {
        await this.dropDowns[0].click();
    }

    public async menuLogout(): Promise<void> {
        await this.dropDowns[1].click();
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    public open(): Promise<string> {
        return super.open('account/home');
    }
}

export default new LoginPage();
