import LoginPage from  '../pageobjects/account.page';
import HomePage from  '../pageobjects/home.page';
import ForgotPasswordPage from  '../pageobjects/forgotpassword.page';

describe('Test', () => {
    var email, password;

    before(() => {
        email = 'grc.dev@yopmail.com';
        password = '123456';
    });

    describe('Invalid Login', () => {
        it('Invalid Email Format', async () => {
            await LoginPage.open();
            await LoginPage.inputEmail('a');

            await expect(LoginPage.invalidInputMsg).toExist();
        });
        it('Email Not Exist', async () => {
            await LoginPage.open();
            await LoginPage.inputEmail('grc.dev2@yopmail.com');
            await LoginPage.inputPassword(password);
            await LoginPage.login();
            await browser.pause(2000);

            await expect(LoginPage.invalidInputMsg).toExist();
        });
        it('Wrong Password', async () => {
            await LoginPage.open();
            await LoginPage.inputEmail(email);
            await LoginPage.inputPassword('111111');
            await LoginPage.login();
            await browser.pause(2000);

            await expect(LoginPage.invalidInputMsg).toExist();
        });
        // it ('Account No Binding', async () => {});
    });
    
    describe('Successful Login', () => {
        it('Login', async () => {
            await LoginPage.open();
            await LoginPage.inputEmail(email);
            await LoginPage.inputPassword(password);
            await LoginPage.login();
            await browser.pause(2000);

            await expect(browser).toHaveUrlContaining('account/home');

            await HomePage.menu();
            await HomePage.menuLogout();
        });
    });

    describe('Check Button', () => {
        it('Furbo Button', async () => {
            await LoginPage.open();
            await LoginPage.gotoFurbo();
            await browser.pause(2000);
            
            await expect(browser).toHaveUrlContaining('furbo.com');
        });
        
        it('Remember Me Button', async () => {
            await LoginPage.open();
            await LoginPage.inputEmail(email);
            await LoginPage.inputPassword(password);
            await LoginPage.rememberMe();
            await LoginPage.login();
            await browser.pause(2000);

            await HomePage.menu();
            await HomePage.menuLogout();
            await browser.pause(2000);

            const autofillEmail = await (LoginPage.email).getValue();
            await expect(autofillEmail).toEqual(email);
        });

        it('Forgot Password Link', async () => {
            await LoginPage.open();
            await LoginPage.gotoForgotPassword();
            await browser.pause(2000);

            await expect(browser).toHaveUrlContaining('forgotpassword');
        });
    });
});