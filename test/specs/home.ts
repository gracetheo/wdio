import LoginPage from  '../pageobjects/account.page';
import HomePage from  '../pageobjects/home.page';

describe('Test', () => {
    var email, password;

    before(() => {
        email = 'grc.dev@yopmail.com';
        password = '123456';
    });

    describe('Login', () => {
        it('Login', async () => {
            await LoginPage.open();
            await LoginPage.inputEmail(email);
            await LoginPage.inputPassword(password);
            await LoginPage.login();
            await browser.pause(2000);
        });
    });

    describe('Check Button', () => {
        it('Furbo Button', async () => {
            await HomePage.open();
            await browser.pause(2000);

            await HomePage.gotoFurbo();

            await expect(browser).toHaveUrlContaining('furbo.com');
        });

        it('Dropdown Menu', async () => {
            await HomePage.open();
            await browser.pause(2000);

            await HomePage.menu();

            await expect(HomePage.dropDowns).toExist();
        });

        it('Subscribe Button', async () => {
            await HomePage.open();
            await browser.pause(2000);

            await HomePage.gotoSub();

            await expect(browser).toHaveUrlContaining('subscribe/furbodognanny');
        });

        it('FDN Button', async () => {
            await HomePage.open();
            await browser.pause(2000);

            await HomePage.gotoFDN();

            await expect(browser).toHaveUrlContaining('home/furbodognanny');
        });
    });

    describe('Logout', () => {
        it('Logout', async () => {
            await HomePage.open();
            await browser.pause(2000);

            await HomePage.menu();
            await HomePage.menuLogout();
            await browser.pause(2000);

            await expect(browser).toHaveUrlContaining('account');
        });
    });
});