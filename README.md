# Run all

    $ yarn wdio

# Run specific test

    $ yarn wdio --spec [specs_filename]

- Example

        $ yarn wdio --spec login.ts